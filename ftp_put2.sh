#!/bin/bash


# --------------------------------------- #
#          Variaveis
# --------------------------------------- #

## Variaveis de identificação do sistema operacional

# Variaveis GLOBAIS
DONE_DIR="/home/wellington/Documentos/pro-ftp/done/"
BACKUP_DIR="/home/wellington/Documentos/pro-ftp/backup/"

# Credenciais do ftp
HOST_FTP="localhost"
USER_FTP="suporte"
PASS_FTP="suporte@123"

# diretório que o arquivo de log ${FILE_LOG}
DIR_LOG="/home/wellington/Documentos/pro-ftp/log/"
FILE_LOG="put.log"

## Variaveis de indentificação do FTP

# Diretório em que serão armazenados os arquivos.
PROCESS_DIR="processados"


## Funções

# LOG
log_put(){
    date=$(date)
    # nome do arquivo dentro do log
    arquivo=$2

    # Se o primeiro parâmetro for -1, considere-se um ERRO
    if [ $1 -eq -1 ]
        then
        string="[${date}] --- [PUT] - Enviando arquivo [ $arquivo ] - [ ER ] : Arquivo não foi enviado com sucesso! "

        echo $string | tee -a ${DIR_LOG}${FILE_LOG}

        exit 0
    fi

    # Se o primeiro parâmetro for -2, considere-se um WARNNING
    if [ $1 -eq -2 ]
        then 
        string="[${date}] --- [PUT] -  [ WARN ] : Não há arquivos para transferencia."

        echo $string | tee -a ${DIR_LOG}${FILE_LOG}

        exit 0
    fi

    string="[${date}] --- [PUT] - Enviando arquivo [ $arquivo ] - [ OK ] - Arquivo enviado com sucesso! "
    
    echo $string | tee -a ${DIR_LOG}${FILE_LOG}
}



# Enviar arquivos para FTP
ftp_put() {
    arquivo=$1
    echo $arquivo
    # Nome que ficará dentro do ftp
    nome_retorno_ftp=$2
    return_ftp="$(ftp -np ${HOST_FTP} <<eof
    quote user ${USER_FTP}
    quote pass ${PASS_FTP}
    cd ${PROCESS_DIR}
    put ${DONE_DIR}${arquivo} ${nome_retorno_ftp}
    bye
eof
)"

# verificando se string está vazia (sem erros no processamento do comando)
if [ -z "${return_ftp}" ] 
    then
        echo "Retorno sem erros"

        ## Enviando o arquivo enviado para diretório de backup
        echo "Transferindo o arquivo para backup"
        
        # log de controle interno status 1 [ OK ]
        log_put 1 $arquivo
        
        cd ${DONE_DIR}

        rename_file=${arquivo}_$(date +%d_%m_%Y_%N)

        mv ${DONE_DIR}${arquivo} ${BACKUP_DIR}${rename_file}
    else
        echo "Houve algum erro no transporte do arquivo"
        # log de controle interno status -1 [ ERR ]
        log_put -1 $arquivo
        exit 0
fi
} ## end função ftp_put

## verificando quantidade de arquivos no dir de done
cd ${DONE_DIR}

qtd_arquivos=$(ls -l | grep ".txt" | wc -l)

# Verificações para log de controle dentro do server
if [ $qtd_arquivos -eq 0 ]
    then
    echo "Não há arquivos no diretório para enviar!"
    
    # log de controle interno status -2 [ WARN ]

    log_put -2 $arquivo

    exit 0
fi

if [ $qtd_arquivos -eq 1 ]
    then
    echo "somente um arquivo!"

    cd ${DONE_DIR}

    # envio ao ftp somente o arquivo existente
    nome_arquivo=$(ls -l | grep .txt | cut -d " " -f9)

    # printando arquivo
    echo $nome_arquivo

    result=$(ftp_put ${nome_arquivo} ${nome_arquivo}_processado)

    echo $result

    exit 0
else
    # Listando arquivos do diretorio DONE_DIR
    ARRAY_ARQUIVOS=$(ls -l | cut -d ' ' -f9)

    for file in $ARRAY_ARQUIVOS
    do
        result=$(ftp_put ${file} ${file}_processado)

        echo $result

        # log de controle interno status [ OK ]
        log_put 1 $file
    done

    exit 0
fi

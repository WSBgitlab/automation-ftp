# Instalação do servidor ftp com docker (Debian)

-- Autor : Wellington da Silva Bezerra
-- version : 1.0
-- env : Desenvolvimento.

Vamos utilizar o docker para facilitar a comunicação local. 
Considerando que o docker já está instalado faça os seguintes steps.

## 1. Instalação da imagem Debian

docker pull debian

## 2. Subindo container com nome de debian-ftp

**docker container run -d -it --name debian-ftp -p 21:21 -p 8080:8080 debian**

## 3. Instalação de FTP

**docker container exec debian-ftp apt-get update**

**docker container exec debian-ftp apt-get install -y vsftpd**

**docker container exec debian-ftp cp -r /etc/vsftpd.conf /etc/vsftpd.conf.ori**

### Atualizaçõões FTP
Vamos atualizar as configurações do FTP, para isso necessário o login dentro do docker.

**docker container attach debian-ftp**

Dentro do container vamos alterar o arquivo de configuração do ftp.

Principais campos alterados nessa configuração.

1. listen=NO
1. listen_ipv6=YES
1. anonymous_enable=NO
1. local_enable=YES
1. write_enable=YES
1. local_umask=022
1. dirmessage_enable=YES
1. use_localtime=YES
1. xferlog_enable=YES
1. connect_from_port_20=YES
1. xferlog_std_format=YES
1. ftpd_banner=Welcome to blah FTP service.
1. chroot_local_user=YES
1. secure_chroot_dir=/var/run/vsftpd/empty
1. pam_service_name=vsftpd
1. rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
1. rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
1. ssl_enable=NO
1. allow_writeable_chroot=YES

Essa opção é apenas para ambiente de teste **allow_writeable_chroot**.

Vamos reiniciar o serviço dentro do container

**docker container exec debian-ftp /etc/init.d/vsftpd start**

### Administrando usuário
Criando usuário suporte dentro do container.

**docker container exec debian-ftp useradd -m -c "suporte, Site" -s /bin/bash suporte**

Vamos entrar no container para digitar a senha do nosso novo usuário.

**docker container attach debian-ftp**

$: passwd suporte

Configurando a senha do usuário, para sair do container pressione CTRL + P + Q.

Vamos para configuração do usuário dentro do FTP.

Verificar se o arquivo existe /etc/vsftpd.userlist,se não criar e executar o comando abaixo

**docker container exec debian-ftp echo "suporte" | tee -a /etc/vsftpd.userlist**

**docker container exec debian-ftp cat /etc/vsftpd.userlist**

**docker container exec debian-ftp usermod -s /sbin/nologin suporte**

**docker container exec debian-ftp chmod a-w /home/suporte**

FROM debian:latest

WORKDIR /tmp/automationftp/

COPY *.sh /tmp/automationftp/

COPY *.md /tmp/automationftp/

COPY *.txt /tmp/automationftp/

COPY *.conf /tmp/automationftp/

RUN mkdir -p backup

RUN mkdir -p done

RUN mkdir -p inject

COPY done/* done/

COPY backup/* backup/

COPY inject/* inject/

RUN apt-get update && apt-get install -y vsftpd

COPY vsftpd.conf /etc/vsftpd.conf

RUN useradd -m -c "suporte,Site" -s /bin/bash suporte

RUN echo 'suporte:suporte@123' | chpasswd

RUN touch /etc/vsftpd.userlist

RUN echo "suporte" | tee -a /etc/vsftpd.userlist

EXPOSE 8080

EXPOSE 21

CMD ["/etc/init.d/vsftpd","start"]

RUN /etc/init.d/vsftpd start

ENTRYPOINT /bin/bash
#!/bin/bash


# --------------------------------------- #
#          Variaveis
# --------------------------------------- #

# Variaveis GLOBAIS
INJECT_DIR="/home/wellington/Documentos/pro-ftp/inject/"

# Credenciais do ftp
HOST_FTP="localhost"
USER_FTP="suporte"
PASS_FTP="suporte@123"

# diretório que o arquivo de log get.log
DIR_LOG="/home/wellington/Documentos/pro-ftp/log/"
FILE_LOG="get.log"


# Diretório em que serão disponibilizado pelo FTP.
TO_PROCESS_DIR="para_processar"
FILE_NAME=$1
NEW_FILE_NAME=$2


## Funções

log_get(){
    date=$(date)
    # nome do arquivo dentro do log
    arquivo=$FILE_NAME

    # Se o primeiro parâmetro for -1, considere-se um ERRO
    if [ $1 -eq -1 ]
        then
        string="[${date}] --- [GET] - Enviando arquivo [ $arquivo ] - [ ER ] : Arquivo não foi transferido com sucesso! "

        echo $string | tee -a ${DIR_LOG}${FILE_LOG}

        exit 0
    fi

    # Se o primeiro parâmetro for -2, considere-se um WARNNING
    if [ $1 -eq -2 ]
        then 
        string="[${date}] --- [GET] -  [ WARN ] : MENSAGEM_DE_WARINING."

        echo $string | tee -a ${DIR_LOG}${FILE_LOG}

        exit 0
    fi

    string="[${date}] --- [GET] - Transferindo o arquivo [ $arquivo ] - [ OK ] - Arquivo transferido com sucesso! "
    
    echo $string | tee -a ${DIR_LOG}${FILE_LOG}
}



ftp_get() {
    # Nome que ficará dentro do ftp
    nome_retorno_ftp=$2
    return_ftp="$(ftp -np ${HOST_FTP} <<eof
    quote user ${USER_FTP}
    quote pass ${PASS_FTP}
    cd ${TO_PROCESS_DIR}
    get ${FILE_NAME} ${INJECT_DIR}${NEW_FILE_NAME}
    bye
eof
)"

# verificando se string está vazia (sem erros no processamento do comando)
if [ -z "${return_ftp}" ] 
    then
        ## Enviando o arquivo enviado para diretório de backup
        echo "Arquivo transferido com sucesso!"
        # log de controle interno
        log_get 1 ${FILE_NAME}
    else
        echo "Houve algum erro no transporte do arquivo"
        log_get -1 ${FILE_NAME}
        exit 0
fi
} #end ftp_get


## RUN
ftp_get

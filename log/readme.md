# Exemplos de Logs

## Método PUT FTP

[ date ] [PUT] - Enviando arquivo [ arquivo ] - [ OK ] : Arquivo enviado com sucesso! <br>
[ date ] [PUT] - Não há arquivos para transferencia. <br>
[ date ] [PUT] - Enviando arquivo [ arquivo ] - [ ER ] : Arquivo não foi enviado com sucesso! <br>


<hr>

## Método GET FTP

[ date ] [GET] - Transferindo arquivo [ arquivo ] - [ OK ] : Arquivo enviado com sucesso! <br>
[ date ] [GET] - MENSSAGEM_WARNNING <br>
[ date ] [GET] - Transferindo arquivo [ arquivo ] - [ ER ] : Arquivo não foi transferido com sucesso! <br>

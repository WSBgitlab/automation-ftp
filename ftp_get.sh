#!/bin/bash
#------------------------------Variaveis-------------------------------

# Dir de destino dentro do sistema operacional
DIR_DESTINO="/home/wellington/tmp/"
# Arquivo de orgigem dentro do sistema operacional
ARQ_ORIGEM="/directory/teste.txt"
HOST_FTP="localhost"
USER_FTP="userftp"
PWD_FTP="12345"
DIR_BKP="backup"

BACKUP_FILE="$ARQ_ORIGEM_`date +%d_%m_%Y`"

#--------------------------------FTP -----------------------------------
echo "Iniciando transferencia..."
cd ${DIR_DESTINO}


echo "HOSTNAME " + $HOST_FTP


<<EOF
open $HOST_FTP
user $USER_FTP 
pass $PWD_FTP 
get $ARQ_ORIGEM
mv ${ARQ_ORIGEM} ${DIR_BKP}${BACKUP_FILE}
bye
EOF

vrc1=$?

if [ $vrc1 -eq 0 ]
then
   echo "Arquivo(s) transmitido(s) com sucesso"
   exit 0
else
   echo "Erro na transmissao do(s) arquivo(s)"
   exit 1
fi
echo "Fim da transferencia..."
#-----------------------------Fim----------------------------------------
